package rest;

import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import io.restassured.RestAssured;

public class RestApiConnector {

    final String BaseURL = "https://api.ratesapi.io/api/";
    public String url = BaseURL;

    public static Response callAPIforGivenURL() {
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.request(Method.GET);
        return response;
    }

    public static void isAPIreturningSuccessResponse() { ;
        String status = callAPIforGivenURL().getStatusLine();
        Assert.assertEquals("API isn't available or wrong URL provided", "HTTP/1.1 200 OK", status);
    }

    public static void isAPIavailable() {
        String responseFormatType = callAPIforGivenURL().getContentType();
        Assert.assertEquals("API doesn't return JSON", "application/json", responseFormatType);
    }

}

