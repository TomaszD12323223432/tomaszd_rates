package StepDefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import rest.Validator;
import rest.RestApiConnector;

public class MyStepdefs {
    RestApiConnector restApiConnector = new RestApiConnector();

    @Given("Rates API for Latest Foreign Exchange rates")
    public void buildAPIurlForLatestRates() {
        RestAssured.baseURI= restApiConnector.url +"latest";
        System.out.println("Default API URI is: "+RestAssured.baseURI);
    }

    @Given("Rates API for {string} Foreign Exchange rates")
    public void buildAPIurlForSpecificDateRates(String specificDate) {
        RestAssured.baseURI= restApiConnector.url +specificDate;
        System.out.println("Default API URI is: "+RestAssured.baseURI);
    }


    @Given("Rates API for Latest Foreign Exchange rates with {string}")
    public void buildAPIurlForWrongAddress(String url) {
        RestAssured.baseURI=url;
        System.out.println("Default API URI: "+RestAssured.baseURI);
    }

    @When("The API is accessible")
    public void the_API_is_available() {
        RestApiConnector.isAPIavailable();
    }

    @Then("An automated test suite is to run that will assert the success status of the response")
    public void verifyResponseStatusIsSuccess() {
        RestApiConnector.isAPIreturningSuccessResponse();
    }





    @Then("The response to {string} request is correct")
    public void correctResponse(String url) {
        Validator.GetAndAssertResponseIncorrectURL(true);
    }

    @Then("An automated test suite is to run for {string} that will assert the response agrees the current date")
    public void positiveTestForFutureDate(String futureDate) {
        Validator.GetAndAssertResponse("", "",futureDate, false);
    }

    @Then("An automated test suite is to run for {string} {string} rate and {string} base that will assert the response")
    public void positiveTestForGivenAPIqueryAndDate(String specificDate,String rates, String base) {
        Validator.GetAndAssertResponse(rates, base,specificDate, false);
    }

    @Then("An automated negative test suite is to run for {string} rate and {string} base that will assert the response")
    public void positiveTestForGivenAPIquery(String rates, String base) {
        try{
            Validator.GetAndAssertResponse(rates, base,null, false);
        }
        catch(Exception E) {
            throw new io.cucumber.java.PendingException();
        }
    }

    @Then("An automated test suite is to run for {string} rate and {string} base that will assert the response")
    public void negativeTestForGivenAPIquery(String rates, String base) {
        Validator.GetAndAssertResponse(rates, base, null, true);
    }
}
