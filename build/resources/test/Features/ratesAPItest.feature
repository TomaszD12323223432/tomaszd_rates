@Test
Feature: Get foreign exchange reference rates.

  Scenario: 1 - Verify if Rates API is accessible
    Given Rates API for Latest Foreign Exchange rates
    When The API is accessible
    Then An automated test suite is to run that will assert the success status of the response


  Scenario Outline: 2 - Verify Rates API response for "<rates>" rate and "<base>"
    Given Rates API for Latest Foreign Exchange rates
    When The API is accessible
    Then An automated test suite is to run for "<rates>" rate and "<base>" base that will assert the response
    Examples:
      | rates                                       | base |
      | PLN                                         | EUR  |
      | GBP                                         | PLN  |
      | EUR,GBP                                     | PLN  |
      | HKD,CHF,CZK,CNY,PLN,CAD,NZD,JPY,RUB,USD,AUD | EUR  |
      |                                             |      |
      | EUR                                         |      |
      | USD                                         |      |
      |                                             | PLN  |

  Scenario Outline: 3 - Verify Rates API response correctness for "<rates>" rate and "<base>"
    Given Rates API for Latest Foreign Exchange rates
    When The API is accessible
    Then An automated negative test suite is to run for "<rates>" rate and "<base>" base that will assert the response
    Examples:
      | rates                            | base    |
      | MXN,CZK,GBP,CAD,AUD,USD,JPY,test |         |
      |                                  | USD,PLN |
      | XXX                              |         |
      | XXX                              | YYY     |
      |                                  | YYY     |


  Scenario Outline: 4 - Verify Rates API response for "<date>" date
    Given Rates API for "<date>" Foreign Exchange rates
    When The API is accessible
    Then An automated test suite is to run that will assert the success status of the response
    Examples:
      | date |
      | 2010-01-12    |
      | 2020-03-15    |


  Scenario Outline: 5 - Verify Rates API response correctness for "<rates>" rate and "<base>" base on "<date>" date
    Given Rates API for "<date>" Foreign Exchange rates
    When The API is accessible
    Then An automated test suite is to run for "<date>" "<rates>" rate and "<base>" base that will assert the response
    Examples:
      | rates   | base | date |
      | USD     | EUR  | 2010-01-12    |
      | GBP     | PLN  | 2020-03-11    |
      | EUR,GBP | PLN  | 2019-04-22    |
      |         |      | 2018-04-01    |
      | USD     |      | 2020-01-01    |
      |         | PLN  | 1999-12-12    |
      | USD,GBP |      | 2010-01-12    |


  Scenario Outline: 6 - Rates API for "<prospective date>" Foreign Exchange rates
    Given Rates API for "<prospective date>" Foreign Exchange rates
    When The API is accessible
    Then An automated test suite is to run for "<prospective date>" that will assert the response agrees the current date
    Examples:
      | prospective date |
      | 2030-01-12  |
      | 2099-03-11  |
      | 2251-04-22  |


  Scenario Outline: 7 - Verify if Rates API is accessible for incorrect "<url>"
    Given Rates API for Latest Foreign Exchange rates with "<url>"
    When The API is accessible
    Then The response to "<url>" request is correct
    Examples:
      | url                                |
      | https://api.ratesapi.io/api/       |
      | https://api.ratesapi.io/           |
      | https://api.ratesapi.io/randomdata |